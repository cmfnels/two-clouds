package tk.secondcloud.twoclouds;

import android.util.Log;

import com.google.gson.Gson;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import tk.secondcloud.twoclouds.weatherData.WeatherData;

public class APIConnection {

    public static final String LOG_TAG = APIConnection.class.getSimpleName();

    public APIConnection(double latitude, double longitude) {

        final OkHttpClient client = new OkHttpClient();
        final String darkSkyKey = "cbc35d03c5f68bd0040b6bbb8423b9ba";
        final String darSkyURL = "https://api.darksky.net/forecast/" + darkSkyKey + "/" + latitude + "," + longitude ;
        final OkHttpClient clientDarkSky = new OkHttpClient();

        Request request = new Request.Builder()
                    .url(darSkyURL)
                    .build();

            clientDarkSky.newCall(request).enqueue(new Callback() {
                @Override
                public void onFailure(Call call, IOException e) {
                    Log.e(LOG_TAG, "Error Connecting");
                    //Display Dialog
                }

                @Override
                public void onResponse(Call call, Response response) throws IOException {
                    if (!response.isSuccessful())
                        throw new IOException("Unexpected code" + response);


                    String jsonData = response.body().string();
                    Gson gson = new Gson();
                    String forecast = jsonData;
                    WeatherData parseData = gson.fromJson(forecast, WeatherData.class);


                    WeatherData.WeatherCurrent parseCurrentData = gson.fromJson(forecast, WeatherData.WeatherCurrent.class);
                    WeatherData.WeatherCurrent current = new WeatherData.WeatherCurrent(
                        parseCurrentData.getSummary(),
                        parseCurrentData.getIcon(),
                        parseCurrentData.getTime(),
                        parseCurrentData.getTemperature()
                    );
                    //BINDING
                    Log.v(LOG_TAG, current.getTemperature().toString());
                }
            });
    }
}

