package tk.secondcloud.twoclouds.weatherData;

public class WeatherData {
    //Currently Values
  private String timezone;
  WeatherCurrent currently;
  WeatherHourly hourly;
  WeatherDaily daily;
  public static class WeatherCurrent{
      String summary;
      String icon;
      long time;
      double temperature;

      public String getSummary() {
          return summary;
      }

      public void setSummary(String summary) {
          this.summary = summary;
      }

      public String getIcon() {
          return icon;
      }

      public void setIcon(String icon) {
          this.icon = icon;
      }

      public Long getTime() {
          return time;
      }

      public void setTime(Long time) {
          this.time = time;
      }

      public Double getTemperature() {
          return temperature;
      }

      public void setTemperature(Double temperature) {
          this.temperature = temperature;
      }

      public WeatherCurrent() {
      }

      public WeatherCurrent(String summary, String icon, long time, double temperature) {
          this.summary = summary;
          this.icon = icon;
          this.time = time;
          this.temperature = temperature;
      }
  }
  public class WeatherHourly{
      String summary;
  }
  public class WeatherDaily{
  }
}
